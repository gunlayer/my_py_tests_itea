from time import perf_counter_ns


class callstat:
    """
    Декоратор, подсчитывающий количество вызовов и 
    среднюю длительность вызова задекорированной функции.

    Пример использования:

    @callstat
    def add(a, b):
        return a + b

    >>> add.call_count
    0
    >>> add(1, 2)
    3
    >>> add.call_count
    1

    Подсказки по реализации: функторы, @property
    Для измерения времени выполнения - perf_counter, см. импорт.

    """
    def __init__(self, fn):
        self.func = fn
        self.__call_count = 0
        self.__func_time = 0
        self.__av_time = 0
     
    @property
    def call_count(self):
        return self.__call_count
    
    @property
    def func_time(self):
        return self.__func_time

    @property
    def av_time(self):
        return self.__av_time


    def __call__(self, *args, **kwargs):
        t1 = perf_counter_ns()
        res = self.func(*args, **kwargs)
        t2 = perf_counter_ns()
        self.__call_count += 1
        self.__func_time += (t2 - t1)
        self.__av_time = self.__func_time / self.__call_count
        return res
